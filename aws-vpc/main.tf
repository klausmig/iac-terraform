resource "aws_instance" "app_server" {
  ami = "ami-04dfd853d88e818e8" #ubuntu

  #ami           = "ami-01be94ae58414ab2e" #amazon-linux
  instance_type = "t2.micro"

  subnet_id                   = element(aws_subnet.public_subnets[*].id, 0)
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.nginx_demo.id]

  user_data = file("userData.tpl")
  tags = {
    Name = "App server in Subnet Public 1"
  }
}

